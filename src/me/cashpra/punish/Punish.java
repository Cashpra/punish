package me.cashpra.punish;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import me.cashpra.punish.managers.CommandManager;
import me.cashpra.punish.managers.EventManager;

public class Punish extends JavaPlugin {

	public void onEnable() {

		initiateManagers();

		getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3&l[PUNISH] &8� &3has been &aenabled."));

	}

	public void onDisbale() {

		initiateManagers();

		getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3&l[PUNISH] &8� &3has been &cdisabled."));

	}

	private void initiateManagers() {

		new CommandManager(this);
		new EventManager(this);
		

	}

}