package me.cashpra.punish.managers;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import me.cashpra.punish.Punish;

public class EventManager implements Listener {

	public Punish plugin;

	org.bukkit.scoreboard.ScoreboardManager manager = Bukkit.getScoreboardManager();

	public EventManager(Punish pl) {

		plugin = pl;

		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);

	}
}