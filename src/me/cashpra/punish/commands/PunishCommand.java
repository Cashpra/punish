package me.cashpra.punish.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.cashpra.punish.Punish;

public class PunishCommand implements CommandExecutor {
	
	public Punish plugin;
	
	public PunishCommand(Punish pl) {
		
		plugin = pl;
		
	}

	@Override
	  public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {

          Player player = (Player) sender;

        	  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----------------------------------------"));
        	  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "          &4&lDexonic Punish Plugin"));
        	  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 "));
        	  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &4Developed By: &fCashpra")); 
        	  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &4Download Link: N/A"));
        	  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8 "));
        	  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----------------------------------------")); 
        	  
		return false;
	}
}