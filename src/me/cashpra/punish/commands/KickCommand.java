package me.cashpra.punish.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.cashpra.punish.Punish;

public class KickCommand implements CommandExecutor {
	
	public Punish plugin;
	
	public KickCommand(Punish pl) {
		
		plugin = pl;
		
	}

	@Override
	  public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
        Player player = (Player) sender; 
        Player target = (Player) Bukkit.getPlayer(args[0]);
        
	if (!sender.hasPermission("punish.kick")) {
		
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l[PERMS] &8� &cYou do not have permissions to access that command!"));

            return true;

        } else {
        	
            if (args.length < 2) {
            
            	player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4&l[ERROR] &8� &4Usage: /kick [player] [reason]"));
            	
            } else {
            	if (!target.isOnline()) {
            		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&8&m-------------------------------------------------- \n &3&l[PUNISH] &8� &3" + target.getDisplayName() + " &fwas &3kicked &fby &3" + player.getDisplayName() + "\n &8&m--------------------------------------------------"));
            		target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&4&lYOU HAVE BEEN REMOVED FROM THE SERVER \n &4Reason &8� &f" + (args[1]) + "\n &4Operator &8� " + player.getDisplayName() + "\n &cAppeal @ appeal.dexonic.net \n \n \n \n \n \n \n \n \n"));
            	} else {
            		
            		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l[CORE] &8� &fThat &6player &fisn't currently &6online&f!"));
            		
            	}
            }
        }
	return false;
	}
}