package me.cashpra.punish.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.cashpra.punish.Punish;

public class UnbanCommand implements CommandExecutor {
	
	public Punish plugin;
	
	public UnbanCommand(Punish pl) {
		
		plugin = pl;
		
	}

	@Override
	  public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
        Player player = (Player) sender; 
        Player target = (Player) Bukkit.getPlayer(args[0]);
        
	if (!sender.hasPermission("punish.ban")) {
		
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l[PERMS] &8� &cYou do not have permissions to access that command!"));

            return true;

        } else {
        	
            if (args.length < 2) {
            
            	player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4&l[ERROR] &8� &4Usage: /ban [player] [reason]"));
            	
            } else {
            	
            	Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&8&m-------------------------------------------------- \n &3&l[PUNISH] &8� &3" + target.getDisplayName() + " &fwas &3unbanned &fby &3" + player.getDisplayName() + "\n &8&m--------------------------------------------------"));
            	Bukkit.unbanIP((args[0]));	
            }
        }	
	return false;
	}
}