package me.cashpra.punish.managers;

import me.cashpra.punish.Punish;
import me.cashpra.punish.commands.BanCommand;
import me.cashpra.punish.commands.KickCommand;
import me.cashpra.punish.commands.PunishCommand;
import me.cashpra.punish.commands.UnbanCommand;
import me.cashpra.punish.commands.WarningCommand;

public class CommandManager {

	private Punish plugin;

	public CommandManager(Punish pl) {

		plugin = pl;

		registerCommands();

	}
	
	private void registerCommands() {
		
		plugin.getCommand("ban").setExecutor(new BanCommand(plugin));
		plugin.getCommand("kick").setExecutor(new KickCommand(plugin));
		plugin.getCommand("punish").setExecutor(new PunishCommand(plugin));
		plugin.getCommand("unban").setExecutor(new UnbanCommand(plugin));
		plugin.getCommand("warn").setExecutor(new WarningCommand(plugin));
			
	}
	
}